﻿#ifndef SPIDER_H
#define SPIDER_H

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QTimer>
#include <QVariant>
#include <QApplication>
#include <QList>
#include <QFileInfo>
#include <QDir>

class Spider : public QObject
{
    Q_OBJECT
public:
    explicit Spider(QObject *parent = nullptr);
    ~Spider();

signals:

public slots:

private slots:
    void httpReadyRead();
    void writeFile(QString path="output.txt", QString data="", QIODevice::OpenModeFlag flag=QIODevice::WriteOnly);
    void get(QUrl url);
    void handleData();                                    //handle dir

    void timeout();

private:
    QNetworkAccessManager *manager;
    QTimer timer;
    QNetworkReply *reply;
    QString all;
    QTimer* m_timerWork;
    int m_pageIndex;
    int m_pageMax;
};

#endif // SPIDER_H
